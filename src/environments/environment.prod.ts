export const environment = {
  production: true,
  app: 'https://creedcms.com/',
  apiUrl: 'https://api-demo.creedcms.com/',
  imageUrl: 'https://api-demo.creedcms.com/Files/',
  firebase: {
    apiKey: 'AIzaSyDMrUNp-Yx6Dafb2xUnm5p-ir0tGEmhpYY',
    authDomain: 'creed-e9329.firebaseapp.com',
    databaseURL: 'https://creed-e9329.firebaseio.com',
    projectId: 'creed-e9329',
    storageBucket: 'creed-e9329.appspot.com',
    messagingSenderId: '308360367656'
  }
};
