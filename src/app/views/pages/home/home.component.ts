import { Component, OnInit } from '@angular/core';
import { SeoService } from 'src/app/services/seo.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private seo: SeoService) { 
    this.seo.generateTags({
      title: 'Home - CREED Church Management System',
      description: 'Read about Creed CMS',
      image: 'assets/img/favour.png'
    });
  }

  ngOnInit() {

  }

}
