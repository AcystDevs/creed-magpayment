import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { NotificationsService } from 'src/app/services/notifications.service';
import { SeoService } from 'src/app/services/seo.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-creed-payment-processing',
  templateUrl: './creed-payment-processing.component.html',
  styleUrls: ['./creed-payment-processing.component.scss']
})
export class CreedPaymentProcessingComponent implements OnInit {

  church: any; pay: any; url = environment;
  constructor(private titleService: Title, public route: ActivatedRoute, private router: Router, 
    public api: ApiService, private seo: SeoService, private notify: NotificationsService) { 
    
  }

  ngOnInit(): void {
    this.init();
  }

  postPayment(){
    
    this.api.post('website/payment',this.pay).subscribe((data: any) => {
      console.info(data);
      this.notify.success({message: 'Request Sent, Kindly Confirm request from your phone'});
      this.init();
    });
  }
  init() {
    // tslint:disable-next-line: max-line-length
    this.pay = { church: 'B0001', type: '', name: '', email: '', source: 'Payment Link', mobile: '', channel: '', amount: '', currency: 'GH¢', reference: '', date: new Date }
  }

}
