import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreedPaymentProcessingComponent } from './creed-payment-processing.component';

describe('CreedPaymentProcessingComponent', () => {
  let component: CreedPaymentProcessingComponent;
  let fixture: ComponentFixture<CreedPaymentProcessingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreedPaymentProcessingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreedPaymentProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
