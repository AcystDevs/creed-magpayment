import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { ToastrModule } from 'ngx-toastr';
import { JwtModule } from '@auth0/angular-jwt';

import { ApiService } from './services/api.service';
import { AuthGuard } from './guards/auth.guard';
import { ExitpageGuard } from './guards/exitpage.guard';
import { ErrorsService } from './services/errors.service';
import { NotificationsService } from './services/notifications.service';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { LoaderInterceptor } from './interceptors/loader.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './views/layout/header/header.component';
import { FooterComponent } from './views/layout/footer/footer.component';
import { SidebarComponent } from './views/layout/sidebar/sidebar.component';
import { LoaderComponent } from './views/layout/loader/loader.component';
import { FunfactComponent } from './views/common/funfact/funfact.component';
import { PartnerComponent } from './views/common/partner/partner.component';
import { ServicesComponent } from './views/pages/services/services.component';
import { NotFoundComponent } from './views/pages/not-found/not-found.component';
import { HomeComponent } from './views/pages/home/home.component';
import { CreedPaymentProcessingComponent } from './views/pages/creed-payment-processing/creed-payment-processing.component';
import { LoginComponent } from './views/pages/login/login.component';
import { DashboardComponent } from './views/pages/dashboard/dashboard.component';

export function tokenGetter() {
  return JSON.parse(localStorage.getItem('Creed'));
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    LoaderComponent,
    FunfactComponent,
    PartnerComponent,
    ServicesComponent,
    NotFoundComponent,
    HomeComponent,
    CreedPaymentProcessingComponent,
    LoginComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
      }
    }),
    AppRoutingModule
  ],
  providers: [ ApiService, AuthGuard, ExitpageGuard, ErrorsService, NotificationsService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    // { provide: ErrorHandler, useClass: ErrorsHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
