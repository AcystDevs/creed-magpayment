import { Injectable } from '@angular/core';
import { Router, Routes, RouterStateSnapshot } from '@angular/router';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpUserEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ErrorsService } from '../services/errors.service';
import { NotificationsService } from '../services/notifications.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    state: RouterStateSnapshot; url: any; cur: any;
    constructor(private router: Router, private jwtHelper: JwtHelperService, private error: ErrorsService, private notify: NotificationsService) {}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.cur = req.url;
        this.url = this.cur.split('/').slice(-2)[0]; // console.log(this.url);
        if (this.url === 'upload') {
            req = req.clone({
                setHeaders: {
                    'Access-Control-Allow-Origin': '*',
                    Authorization: `Bearer ` + this.jwtHelper.tokenGetter()
                }
            });
        } else {
            req = req.clone({
                setHeaders: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    Authorization: `Bearer ` + this.jwtHelper.tokenGetter()
                }
            });
        }
        // this.error.request(req);
        return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
                // do stuff with response if you want
                // console.log(event);
                if (event.body && event.ok) {
                    if(event.body.message) { 
                        this.notify.success(event.body); 
                    }
                }
            }
        }, (err: any) => {
            // this.error.log(err); // console.log(err);
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    // this.notify.notify('Login Session Expired Please Login Again');
                    // auto logout if 401 response returned from api
                    // this.notify.error('End');
                    // localStorage.removeItem('Creed');
                    // location.reload(true);
                    // this.router.navigate(['/login'], { queryParams: { returnUrl: this.state.url } });
                }
            }
            const msg = err.error.message || err.message;
            this.notify.error(msg);
            // this.notify.error(msg);
        }));
    }
}

